//
//  WeatherTests.swift
//  SupaWeather2000
//
//  Created by Idriss on 05/07/2016.
//  Copyright © 2016 idriss. All rights reserved.
//

import XCTest

@testable import SupaWeather2000

class WeatherTests: XCTestCase {

    func testWithCorrectJson() {
        
        let item = dict(with: "rain")
        if let weather = Weather(with: item) {
            XCTAssertNotNil(weather)
        } else {
            XCTFail()
        }
        
    }
    
    func testCorrectConditions() {
        
        let conds = ["rain", "thunderstorm", "drizzle", "snow", "clear", "clouds", "something"]
        let expected : [Condition] = [.Rain, .Thunderstorm, .ShowerRain, .Snow, .Clear, .Clouds, .Clouds]
        
        for i in 0..<conds.count {
            
            let item = dict(with: conds[i])
            if let weather = Weather(with: item) {
                XCTAssertNotNil(weather)
                XCTAssert(weather.condition == expected[i])
            } else {
                XCTFail()
            }
            
        }
    }
    
    func testIncorrectJson() {
        let item : [String: AnyObject] = ["dt":Int(NSDate().timeIntervalSince1970),
                    "temp":["min":Double(1.0)],
                    "weather":[["main":"rain"]]]
        if Weather(with: item) != nil {
            XCTFail()
        } else {
            XCTAssert(true)
        }
        
        

    }
    
    private func dict(with weatherCondition: String) -> [String:AnyObject] {
        
        return ["dt":Int(NSDate().timeIntervalSince1970),
                "temp":["min":Double(1.0),
                    "max":Double(2.0)],
                "weather":[["main":weatherCondition]]]
        
    }


}
