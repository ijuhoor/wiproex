//
//  WeatherResourceTests.swift
//  SupaWeather2000
//
//  Created by Idriss on 05/07/2016.
//  Copyright © 2016 idriss. All rights reserved.
//

import XCTest

@testable import SupaWeather2000

class WeatherResourceTests: XCTestCase {

    func testResource() {
        
        /* Here we are not trying to test the JSON PArsing but that it the parsing function is executed*/
        
        let weatherResource = Resource<[Weather]>(url: NSURL(string: "http://testURL.com/test")!) { json in
            guard let weatherItems = json["list"] as? [JsonDictionary] else { return []}
            return weatherItems.flatMap(Weather.init)
        }
        
        let path = NSBundle(forClass: WeatherResourceTests.self).pathForResource("testResource", ofType:"json")
        let data = NSData(contentsOfFile: path!)!
        let parsedData = weatherResource.parse(data)
        
        XCTAssertNotNil(parsedData)
        
    }
     
}

class WeatherResourceAPITests: XCTestCase {
    
    func testFiveDaysForecast() {
        let resource = WeatherResourceAPI(appid: "12345").fiveDayForecast(for: "Ouagadougou")
        XCTAssertNotNil(resource)
        
        XCTAssertEqual(resource!.url.absoluteString, "http://api.openweathermap.org/data/2.5/forecast/daily?q=Ouagadougou&units=metric&cnt=5&APPID=12345")
        
    }
    
    func testFiveDaysJsonParsing() {
        let resource = WeatherResourceAPI(appid: "12345").fiveDayForecast(for: "Ouagadougou")
        XCTAssertNotNil(resource)

        let path = NSBundle(forClass: WeatherResourceTests.self).pathForResource("testResource", ofType:"json")
        let data = NSData(contentsOfFile: path!)!
        let parsedData = resource!.parse(data)
        
        XCTAssertNotNil(parsedData)
        
    }
    
    func testFiveDaysFailed() {
        let resource = WeatherResourceAPI(appid: "12345").fiveDayForecast(for: "\n\n<br>¡™¡™∂¡")
        XCTAssertNil(resource)
        
        
    }
    
    func testFiveDaysFailsJsonParsing() {
        let resource = WeatherResourceAPI(appid: "12345").fiveDayForecast(for: "Ouagadougou")
        XCTAssertNotNil(resource)
        
        let path = NSBundle(forClass: WeatherResourceTests.self).pathForResource("malformedResource", ofType:"json")
        let data = NSData(contentsOfFile: path!)!
        let parsedData = resource!.parse(data)
        
        XCTAssertNil(parsedData)
        
    }
    
}
