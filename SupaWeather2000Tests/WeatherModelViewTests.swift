//
//  WeatherModelViewTests.swift
//  SupaWeather2000
//
//  Created by Idriss on 05/07/2016.
//  Copyright © 2016 idriss. All rights reserved.
//

import XCTest

@testable import SupaWeather2000

class WeatherModelViewTests: XCTestCase {

    func testInit() {
    
        let weather = Weather(date: NSDate(),
                              condition: .Clear,
                              minTemperature: 0,
                              maxTemperature: 12)
        
        let modelView = WeatherModelView(weather: weather)
        XCTAssertNotNil(modelView)
        
    }
    
    func testMinimumTempPositive() {
     
        let weather = Weather(date: NSDate(),
                              condition: .Clear,
                              minTemperature: 10,
                              maxTemperature: 12)
        
        let modelView = WeatherModelView(weather: weather)
        XCTAssertNotNil(modelView)
        
        XCTAssertEqual("min 10ºC", modelView.minimumTemperature())
    }
    
    func testMinimumTempNegative() {
        
        let weather = Weather(date: NSDate(),
                              condition: .Clear,
                              minTemperature: -9,
                              maxTemperature: 12)
        
        let modelView = WeatherModelView(weather: weather)
        XCTAssertNotNil(modelView)
        
        XCTAssertEqual("min -9ºC", modelView.minimumTemperature())
    }
    
    func testMaximumTempPositive() {
        
        let weather = Weather(date: NSDate(),
                              condition: .Clear,
                              minTemperature: 10,
                              maxTemperature: 12)
        
        let modelView = WeatherModelView(weather: weather)
        XCTAssertNotNil(modelView)
        
        XCTAssertEqual("max 12ºC", modelView.maximumTemperature())
    }
    
    func testMaximumTempNegative() {
        
        let weather = Weather(date: NSDate(),
                              condition: .Clear,
                              minTemperature: -20,
                              maxTemperature: -10)
        
        let modelView = WeatherModelView(weather: weather)
        XCTAssertNotNil(modelView)
        
        XCTAssertEqual("max -10ºC", modelView.maximumTemperature())
    }

    func testDateToday() {
        
        let weather = Weather(date: NSDate(),
                              condition: .Clear,
                              minTemperature: -20,
                              maxTemperature: -10)
        
        let modelView = WeatherModelView(weather: weather)
        XCTAssertNotNil(modelView)
        
        XCTAssertEqual("Today", modelView.date())
    }
    
    func testDateTomorrow() {
        let weather = Weather(date: NSDate().dateByAddingTimeInterval(3600 * 24),
                              condition: .Clear,
                              minTemperature: -20,
                              maxTemperature: -10)
        
        let modelView = WeatherModelView(weather: weather)
        XCTAssertNotNil(modelView)
        
        XCTAssertEqual("Tomorrow", modelView.date())
    }
    
    func testDateFarFuture() {
        let weather = Weather(date: NSDate(timeIntervalSince1970: 1499250881), // Wed, 05 Jul 2017 10:34:41 GMT
                              condition: .Clear,
                              minTemperature: -20,
                              maxTemperature: -10)
        
        let modelView = WeatherModelView(weather: weather)
        XCTAssertNotNil(modelView)
        
        XCTAssertEqual("Jul 5, 2017", modelView.date())
    }
    
    func testIconNames() {
        
        let conditions : [Condition] = [.Clear, .FewClouds, .Clouds, .BrokenClouds, .Mist, .Rain, .ShowerRain, .Snow, .Thunderstorm]
        let expected   = ["Clear", "FewClouds", "Clouds", "BrokenClouds", "Mist", "Rain", "ShowerRain", "Snow", "Thunderstorm"]
        
        for i in 0..<conditions.count {
            
            let condition = conditions[i]
            let weather = Weather(date: NSDate(),
                                  condition: condition,
                                  minTemperature: -20,
                                  maxTemperature: -10)
            
            let modelView = WeatherModelView(weather: weather)
            XCTAssertNotNil(modelView)
            XCTAssertEqual(expected[i], modelView.iconName())
            
        }
        
    }
    
}
