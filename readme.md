# Coding Exercise

#Dependencies

* Swift 2.2: I was not sure you'll have the beta environment setup

No other dependencies (pods or other)

#Build

#### Using xcode: 
Just run the project

#### Using command line:

	xcodebuild -project SupaWeather2000.xcodeproj -target SupaWeather2000 build
	
#Testing

#### Using xcode: 
Just run the test target of the project

#### Using command line:
	xcodebuild -project SupaWeather2000.xcodeproj -scheme SupaWeather2000 -sdk iphonesimulator test
	
#Todo

Extra things that can be done:

* Property File for the API KEY etc...
* Add Unit Support (Farenheit VS Celcius)
* Different sources (Dark Sky etc...)
* Retry on failed to get the data
* Add location Selector
* Add Language Support

