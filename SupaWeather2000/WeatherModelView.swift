//
//  WeatherModelView.swift
//  SupaWeather2000
//
//  Created by Idriss on 05/07/2016.
//  Copyright © 2016 idriss. All rights reserved.
//

import Foundation

class WeatherModelView {
    
    let weather: Weather
    
    private let dateFormatter: NSDateFormatter = {
        let dateFormatter = NSDateFormatter()
        dateFormatter.timeStyle = .NoStyle
        dateFormatter.dateStyle = .MediumStyle
        dateFormatter.doesRelativeDateFormatting = true
        return dateFormatter
    }()
    
    init(weather:Weather) {
        
        self.weather = weather
    }
    
    private func temperature(from temp:Double) -> String {
        
        let formattedNumber = String(format: "%0.0f", temp)
        return "\(formattedNumber)ºC"
        
    }
    
    func minimumTemperature() -> String {
        
        let temp = weather.minTemperature
        return "min \(temperature(from: temp))"
        
    }
    
    func maximumTemperature() -> String {
        
        let temp = weather.maxTemperature
        return "max \(temperature(from: temp))"
        
    }

    func date() -> String {
        
        return dateFormatter.stringFromDate(weather.date)
        
    }
    
    func iconName() -> String {
        
        switch weather.condition {
        case .Clear:
            return "Clear"
        case .FewClouds:
            return "FewClouds"
        case .Clouds:
            return "Clouds"
        case .BrokenClouds:
            return "BrokenClouds"
        case .Mist:
            return "Mist"
        case .Rain:
            return "Rain"
        case .ShowerRain:
            return "ShowerRain"
        case .Snow:
            return "Snow"
        case .Thunderstorm:
            return "Thunderstorm"
        }
        
    }
    
}