//
//  WeatherResourceAPI.swift
//  SupaWeather2000
//
//  Created by Idriss on 05/07/2016.
//  Copyright © 2016 idriss. All rights reserved.
//

import Foundation

public class WeatherResourceAPI {
    
    let baseURL = NSURL(string: "http://api.openweathermap.org/data/2.5/forecast/")
    let appid: String
    
    init(appid: String) {
        self.appid = appid
    }
    
    func fiveDayForecast(for cityName: String) -> Resource<[Weather]>? {
        
        guard let fiveDayForecastURL = NSURL(string: "daily?q=\(cityName)&units=metric&cnt=5&APPID=\(appid)",
                                             relativeToURL: baseURL) else { return nil }
        return Resource<[Weather]>(url: fiveDayForecastURL, jsonParser: { json -> [Weather]? in
            guard let weatherItems = json["list"] as? [JsonDictionary] else { return nil}
            return weatherItems.flatMap(Weather.init)
        })
        
    }
    
}
