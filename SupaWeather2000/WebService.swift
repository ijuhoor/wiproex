//
//  WebService.swift
//  SupaWeather2000
//
//  Created by Idriss on 05/07/2016.
//  Copyright © 2016 idriss. All rights reserved.
//

import Foundation

typealias JsonDictionary = [String: AnyObject]

struct Resource<T> {
    let url : NSURL
    let parse: (NSData) -> T?
}

extension Resource {
    
    init(url: NSURL, jsonParser: AnyObject -> T?) {
        self.url   = url
        self.parse = { data in
            let json = try? NSJSONSerialization.JSONObjectWithData(data, options: [])
            return json.flatMap(jsonParser)
        }
    }
    
}


class WebService {

    func load<T>(resource: Resource<T>, completion: (T?, NSError?)-> Void) {
        let task = NSURLSession.sharedSession().dataTaskWithURL(resource.url,
                                                                completionHandler: { data, response, error in
                                                                    let result = data.flatMap(resource.parse)
                                                                    dispatch_async(dispatch_get_main_queue(), {
                                                                        completion(result, error)
                                                                    })
        })
        task.resume()
    }
    
}

