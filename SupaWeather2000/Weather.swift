//
//  Weather.swift
//  SupaWeather2000
//
//  Created by Idriss on 05/07/2016.
//  Copyright © 2016 idriss. All rights reserved.
//

import Foundation

public enum Condition {
    case Clear
    case FewClouds
    case Clouds
    case BrokenClouds
    case ShowerRain
    case Rain
    case Thunderstorm
    case Snow
    case Mist
}


public struct Weather {
    public let date: NSDate
    public let condition: Condition
    public let minTemperature: Double
    public let maxTemperature: Double
}

extension Weather {
    
    public init?(with jsonDict: [String: AnyObject]) {
        
        guard let epoch = jsonDict["dt"] as? Int,
            let min = jsonDict["temp"]?["min"] as? Double,
            let max = jsonDict["temp"]?["max"] as? Double,
            let cond = jsonDict["weather"]?[0]?["main"] as? String  else { return nil }
        
        self.date = NSDate(timeIntervalSince1970: Double(epoch))
        self.minTemperature = min
        self.maxTemperature = max
        self.condition = Weather.condition(from: cond)
        
    }
    
    private static func condition(from description:String) -> Condition {
    
        switch description {
        case "rain":
            return .Rain
        case "thunderstorm":
            return .Thunderstorm
        case "drizzle":
            return .ShowerRain
        case "snow":
            return .Snow
        case "clear":
            return .Clear
        case "clouds":
            return .Clouds
        default:
            return .Clouds /*In UK when we don't know, it's always cloudy ;) */
        }
        
    }
    
}