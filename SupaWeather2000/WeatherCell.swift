//
//  WeatherCell.swift
//  SupaWeather2000
//
//  Created by Idriss on 05/07/2016.
//  Copyright © 2016 idriss. All rights reserved.
//

import UIKit

class WeatherCell: UITableViewCell {
    
    @IBOutlet var date: UILabel!
    @IBOutlet var minTemp: UILabel!
    @IBOutlet var maxTemp: UILabel!
    @IBOutlet var weatherIcon: UIImageView!
}
