//
//  WeatherViewController.swift
//  SupaWeather2000
//
//  Created by Idriss on 05/07/2016.
//  Copyright © 2016 idriss. All rights reserved.
//

import UIKit

enum WeatherCellType : String {
    case Normal = "weatherCellNormalID"
}

let appId = "01e8cfbfe0b831fbe96add00741ee151"
let city  = "London"

class WeatherViewController: UIViewController {

    @IBOutlet var tableView: UITableView!
    @IBOutlet var message: UILabel!
    @IBOutlet var spinner: UIActivityIndicatorView!

    var forecast: [Weather] = [Weather]()
    
    override func viewDidLoad() {
        
        tableView.dataSource = self
        tableView.delegate   = self
        
        let cellNib = UINib(nibName: "WeatherCell", bundle: nil)
        tableView.registerNib(cellNib, forCellReuseIdentifier: cellIDForType(.Normal))
        
        loadWeather()
        
    }
    
    func cellIDForType(type: WeatherCellType) -> String {
        return type.rawValue
    }
    
    func loadWeather() {
        
        showLoading(true)
        
        if let forecastResource = WeatherResourceAPI(appid: appId).fiveDayForecast(for: city) {
            WebService().load(forecastResource, completion: { [unowned self] weatherForecast, error in

                if let weatherForecast = weatherForecast {
                    self.forecast = weatherForecast
                    self.showWeather()
                    
                } else {
                    self.showError(error)
                }
            })
        } else {
            showError(nil)
        }
        
    }
    
    func showLoading(show: Bool) {

        if show {
            spinner.startAnimating()
            message.text = "Fetching weather"

        } else {
            spinner.stopAnimating()
            message.text = nil
            
        }
    }
    
    func showError(error: NSError?) {
        
        /* Here we can show a more appropriate message on how to
            solve the situation. For the exercise we'll just show
            the error message
         */
        showLoading(false)
        
        guard error != nil else {
            message.text = "Oops something strange happened."
            return
        }
        
        message.text = error?.localizedDescription
        
    }
    
    func showWeather() {
        
        showLoading(false)

        self.tableView.reloadData()
        self.tableView.hidden = false
        
    }
    
}


extension WeatherViewController : UITableViewDataSource {
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return forecast.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCellWithIdentifier(cellIDForType(.Normal)) as? WeatherCell else { fatalError("cell type not set")}

        let formatter     = WeatherModelView(weather: forecast[indexPath.row])
        cell.date.text    = formatter.date()
        cell.maxTemp.text = formatter.maximumTemperature()
        cell.minTemp.text = formatter.minimumTemperature()
        
        if let image = UIImage(named: formatter.iconName()) {
            cell.weatherIcon.image = image
        } else {
            cell.weatherIcon.image = nil
        }
        
        return cell
    }
    
}

extension WeatherViewController : UITableViewDelegate {
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return CGFloat(80.0)
    }
    
}